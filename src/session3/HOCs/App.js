import React from 'react'
import Tasks from './Tasks'

class App extends React.Component {
  render () {
    return (
      <div>
        My app
        <hr/>
        <Tasks permission="view_task_list"/>
      </div>
    );
  }
}

export default App