import React from 'react'

const user = {
  name: 'Ali',
  family: 'Mousavi',
  permissions: ['view_task_list', 'add_task'],
  // role: ['admin', 'operator']
}

function ACL(Component) {
  return class MyACL extends React.Component {
    render () {
      return user.permissions.includes(this.props.permission) && <Component {...this.props} />
    }
  }
}

export default ACL

