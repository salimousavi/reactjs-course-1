import React from 'react'

class Form extends React.Component {

  constructor () {
    super();
    this.state = {
      username: '',
      password: ''
    }

    this.handleChangeUsername = this.handleChangeUsername.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChangeUsername(event) {
    this.setState({username: event.target.value})
  }

  handleChangePassword(event) {
    this.setState({password: event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault()
    console.log(this.state)
    this.setState({
      username: '',
      password: '',
    })
  }

  render () {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>

          <label>
            username:
            <input type="text" value={this.state.username} onChange={this.handleChangeUsername}/>
          </label>

          <label>
            password:
            <input type="text" value={this.state.password} onChange={this.handleChangePassword}/>
          </label>


          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

export class AdvancedForm extends React.Component {
  constructor () {
    super();
    this.state = {
      username: '',
      password: '',
    }


    const obj = {
      name: 'Ali',
      family: 'Mousavi',
      fullName: 'Ali Mousavi',
      age: 30
    }


    obj.name = 'Hasan'
    obj['family'] = 'Hasani'

    const propertyName = 'family'

    console.log(obj[propertyName])

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChangeInput = this.handleChangeInput.bind(this)
  }

  handleChangeInput(event) {
    console.log(event.target.name, event.target.value)
    this.setState({[event.target.name]: event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault()
    console.log(this.state)
  }

  render () {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>

          <label>
            username:
            <input name="username" type="text" value={this.state.username} onChange={this.handleChangeInput}/>
          </label>

          <label>
            password:
            <input name="password" type="text" value={this.state.password} onChange={this.handleChangeInput}/>
          </label>


          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}


export default Form
