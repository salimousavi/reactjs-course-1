import React from 'react'

class App extends React.Component {
  constructor () {
    super();
    this.state = {
      counter: 0,
      status: 0
    }
  }

  render () {
    return (
      <div>
        <div>Countr: {this.state.counter}</div>
        <div>Status: {this.state.status}</div>

        <button onClick={() => this.setState(state => ({counter: state.counter + 1}))}>Increase counter</button>
        <button onClick={() => this.setState(state => ({status: state.status + 1}))}>Increase status</button>
        <hr/>

        <MyComponent counter={this.state.counter}/>
        <MyPureComponent counter={this.state.counter} />
      </div>
    );
  }
}

class MyPureComponent extends React.PureComponent {

  render () {
    console.log("My PureComponent render", this.props.counter)
    return (
      <div>
        My PureComponent {this.props.counter}
      </div>
    );
  }
}


class MyComponent extends React.Component {

  // shouldComponentUpdate (nextProps, nextState, nextContext) {
  //   console.log("shouldComponentUpdate", nextProps.counter === this.props.counter)
  //   if (nextProps.counter === this.props.counter) {
  //     return false
  //   }
  //   return true
  // }

  render () {
    console.log('My Component render', this.props.counter)
    return (
      <div>
        My Component {this.props.counter}
      </div>
    );
  }
}

export default App