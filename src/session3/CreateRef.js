import React from 'react'

class CreateRef extends React.Component {

  constructor () {
    super();
    this.state = {
      showSearchBox: false
    }

    this.textInput = React.createRef()
  }

  componentDidMount () {
    this.textInput.current.focus()
  }

  activeSearchBox() {
    this.setState({showSearchBox: true}, () => this.textInput.current.focus())
  }

  render () {
    return (
      <div>
        {/*<input style={{display: this.state.showSearchBox ? 'inline' : 'none'}} type="text" ref={this.textInput}/>*/}
        <input type="text" ref={this.textInput}/>
        <button onClick={() => this.activeSearchBox()}>Search</button>
      </div>
    );
  }
}

export default CreateRef
