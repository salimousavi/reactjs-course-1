import React, { useContext } from 'react'
import ThemeContext from './ThemeContext'

function Home () {

  const theme = useContext(ThemeContext)

  return (
    <article style={{ flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <h1>My Home Page</h1>
      <p>
        Lorem ipsum dolor sit amet, est an fugit efficiantur. Et magna explicari argumentum mei, usu homero quidam no,
        usu mutat forensibus deseruisse at. Id prompta nostrum mediocritatem quo. Vidit dolorem cum id. An inimicus
        disputationi pro. Ne integre legimus per, ius graecis sensibus at, at choro mentitum vis.
      </p>
      <p>
        Qualisque principes ne qui. Mea ea essent omittam percipitur. In pri liber legere dolorum. Vim cu zril appareat
        partiendo, summo reprimique eu nec. Ut pri accumsan evertitur, recusabo scripserit ex quo. Duo oportere
        deterruisset ne.
      </p>
    </article>
  )
}

export default Home
