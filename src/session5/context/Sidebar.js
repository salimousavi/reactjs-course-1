import React from 'react'
import {NavLink} from 'react-router-dom'
import ThemeContext from './ThemeContext'
import './sidebar.css'

class Sidebar extends React.Component {

  static contextType = ThemeContext

  render () {
    return (
      <aside style={{flex: 1, border: 'solid 1px', padding: '1em', margin: '1em', ...this.context}}>
        <h2>
          My Sidebar
        </h2>

        <ul>
          <li>
            <NavLink exact to="/"> Home </NavLink>
            {/*<a href="/"> Home </a>*/}
          </li>
          <li>
            <NavLink to="/about"> About </NavLink>
            {/*<a href="/about"> About </a>*/}
          </li>
          <li>
            <NavLink to="/post/1"> Post 1 </NavLink>
          </li>
          <li>
            <NavLink to="/post/2"> Post 2 </NavLink>
          </li>
          <li>
            <NavLink to="/post/3"> Post 3 </NavLink>
          </li>
          <li>
            <NavLink to="/post/4"> Post 4 </NavLink>
          </li>
          <li>
            <NavLink to="/profile"> Profile </NavLink>
          </li>
        </ul>
      </aside>
    )
  }
}

export default Sidebar
