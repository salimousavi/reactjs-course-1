import React from 'react'
import ThemeContext from './ThemeContext'
import UserContext from './UserContext'
import { Link } from 'react-router-dom'

class Header extends React.Component {

  render () {
    return (
      <UserContext.Consumer>
        {userContext =>
          <ThemeContext.Consumer>
            {theme =>
              <header style={{border: 'solid 1px', padding: '1em', margin: '1em', ...theme, display: 'flex', justifyContent: 'space-between'}}>
                <h2>Header</h2>
                {userContext.authenticated ?
                  <span>
                    Hi {userContext.user.name}
                    <span style={{color: 'red', cursor: 'pointer'}} onClick={() => userContext.logout()}> Logout => </span>
                  </span> :
                  <Link to="/login"> => Login </Link>
                }
              </header>
            }
          </ThemeContext.Consumer>
        }
      </UserContext.Consumer>
    )
  }
}

export default Header
