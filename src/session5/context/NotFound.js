import React, { useContext } from 'react'
import ThemeContext from './ThemeContext'
import { Link } from 'react-router-dom'

function NotFound () {

  const theme = useContext(ThemeContext)

  return (
    <article style={{ flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <h1>404 page</h1>
      <p>
        Go to home page => <Link to="/">Home</Link>
      </p>
    </article>
  )
}

export default NotFound
