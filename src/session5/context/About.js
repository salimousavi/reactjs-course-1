import React, { useContext } from 'react'
import ThemeContext from './ThemeContext'
// import { Redirect } from 'react-router'

function About () {

  const theme = useContext(ThemeContext)

  // return <Redirect to="/" />

  return (
    <article style={{ flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <h1>About page</h1>
      <p>
        My about page
      </p>
    </article>
  )
}

export default About
