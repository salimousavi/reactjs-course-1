import React, { useContext } from 'react'
import { Redirect, useHistory, useLocation } from 'react-router'
import ThemeContext from './ThemeContext'
import UserContext from './UserContext'

function Login () {

  const theme = useContext(ThemeContext)
  const {login, authenticated} = useContext(UserContext)

  const history = useHistory()
  const location = useLocation()

  console.log(location)

  function singIn () {
    const {oldPath} = location.state || {oldPath: '/'}

    login()
    history.push(oldPath)
  }

  if (authenticated) {
    return <Redirect to="/"/>
  }

  return (
    <article style={{flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      You must be login .!!!
      <hr/>
      <button onClick={() => singIn()}>Login</button>
    </article>
  )
}

export default Login
