import React, { useContext } from 'react'
import ThemeContext from './ThemeContext'
import UserContext from './UserContext'

function Profile () {

  const theme = useContext(ThemeContext)
  const {user} = useContext(UserContext)

  return (
    <article style={{ flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <h1>My Profile</h1>
      <p>
        My name is {user.name}
      </p>
    </article>
  )
}

export default Profile
