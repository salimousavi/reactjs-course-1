import React, { useEffect, useState } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import cookie from 'js-cookie'
import Header from './Header'
import Home from './Home'
import About from './About'
import Profile from './Profile'
import Login from './Login'
import NotFound from './NotFound'
import Post from './Post'
import Sidebar from './Sidebar'
import Footer from './Footer'
import PrivateRoute from './PrivateRoute'
import ThemeContext, { themes } from './ThemeContext'
import UserContext from './UserContext'

function App () {

  const [theme, setTheme] = useState(themes.light)
  const [user, setUser] = useState({})
  const [authenticated, setAuthenticated] = useState(false)
  const [loading, setLoading] = useState(true)

  // useEffect(() => {
  //   fetch('https://jsonplaceholder.typicode.com/users/1')
  //     .then(res => res.json())
  //     .then(result => setUser(result))
  // }, [])

  useEffect(() => {
    // const token = cookie.get('token')
    // if (token) {
    //   login()
    // }
    // else {
    //   setLoading(false)
    // }
    cookie.get('token') ? login() : setLoading(false)
  }, [])

  function login () {
    setLoading(true)
    fetch('https://jsonplaceholder.typicode.com/users/1')
      .then(res => res.json())
      .then(result => {
        cookie.set('token', 'my-token', {expires: 7})
        setUser(result)
        setAuthenticated(true)
      })
      .finally(() => setLoading(false))
  }

  function logout () {
    cookie.remove('token')
    setUser({})
    setAuthenticated(false)
  }

  if (loading) return 'Loading ...'

  return (
    <div>
      <BrowserRouter>
        <UserContext.Provider value={{user, authenticated, login, logout}}>
          <ThemeContext.Provider value={theme}>
            <div>
              {
                theme === themes.light ?
                  <button onClick={() => setTheme(themes.dark)}>Dark</button> :
                  <button onClick={() => setTheme(themes.light)}>Light</button>
              }
            </div>
            <Header/>
            <section style={{display: 'flex'}}>
              <Sidebar/>
              <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/about" component={About}/>
                <Route path="/post/:id" component={Post}/>
                <PrivateRoute path="/profile" component={Profile} />
                {/*<PrivateRoute path="/profile"> <Profile/> </PrivateRoute>*/}
                <Route path="/login" component={Login}/>
                {/*<Route path="/login" render={props => <Login {...props} />}/>*/}
                <Route path="*" component={NotFound}/>
                <Redirect to="/"/>
              </Switch>
            </section>
            <Footer/>
          </ThemeContext.Provider>
        </UserContext.Provider>
      </BrowserRouter>
    </div>
  )
}

export default App
