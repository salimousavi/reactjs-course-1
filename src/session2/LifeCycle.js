import React from 'react'

class LifeCycle extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      counter: 1
    }
    console.log("constructor")
  }

  componentDidMount () {
    console.log("componentDidMount")
    // setTimeout(() => this.setState({counter: 11}), 2000)
  }

  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedStateFromProps", props, state)
    if (state.counter % 2) {
      return {
        counter: state.counter + 1
      }
    }

    return null
  }

  shouldComponentUpdate (nextProps, nextState, nextContext) {
    console.log("shouldComponentUpdate", nextProps, nextState)
    if (nextState.counter %10 === 0) {
      return false
    }

    return true
  }

  getSnapshotBeforeUpdate (prevProps, prevState) {
    console.log("getSnapshotBeforeUpdate", prevState)
    if (prevState.counter === 22) {
      return null
    }
    return 'ali'
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    console.log("componentDidUpdate")
    console.log(prevProps, this.props)
    console.log(prevState, this.state)
    console.log(snapshot)
    if (snapshot === 'ali') {
      this.setState({counter: 22})
    }
  }

  render () {
    console.log('render')
    return <div>
      {this.state.counter}
      <button onClick={() => this.setState({counter: this.state.counter + 1})} > Increment </button>
    </div>
  }

}

export default LifeCycle
