import React from 'react';
import Counter from './Counter'
import Title from './Title'
import { Title as AdminTitle } from './admin/Title'

class App extends React.Component {

  constructor () {
    super();
    this.state = {
      initCounter: 12
    }
  }

  componentDidMount () {
    setTimeout(() => this.setState({initCounter: 0}), 1000)
  }

  render () {
    console.log("this.state.initCounter = " ,this.state.initCounter)
    return (
      <div>
        <Counter/>
        <hr />
        <Counter counter={this.state.initCounter}/>

        {/*<Title/>*/}
        {/*<AdminTitle name="Eli" color="green"/>*/}
        {/*<Title name="Mahsa" color={"blue"}/>*/}
      </div>
    );
  }
}

export default App;
