import React, {Component} from 'react'

class Title extends Component {

  render () {
    console.log(this.props)
    return <h1 style={{color: this.props.color, marginTop: '50px'}}>
      {this.props.name}
    </h1>
  }
}

Title.defaultProps = {
  color: 'red',
  name: 'My Title'
}

export default Title
