import React from 'react'

class Item extends React.Component {
  render () {
    return (
      <li>
        {this.props.name}
      </li>
    );
  }
}

class ListItems extends React.Component {

  state = {
    items: ['Ali', 'Mahsa', 'Eli']
  }

  // records = [
  //   {
  //     id: 1,
  //     name: 'Ali'
  //   },
  //   {
  //     id: 2,
  //     name: 'Eli'
  //   }
  // ]

  render () {
    const {items} = this.state
    return (
      <ul>
        {items.map((item, index) => <Item key={index} name={item}/>)}
        {/*<Item name={items[0]}/>*/}
        {/*<Item name={'Morteza'}/>*/}
        {/*<Item name={items[2]}/>*/}
      </ul>
    )

  }
}

export default ListItems