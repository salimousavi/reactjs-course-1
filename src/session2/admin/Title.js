import React, {Component} from 'react'

export class Title extends Component {

  render () {
    return <h3 style={{color: this.props.color, marginTop: '50px'}}>
      {this.props.name}
    </h3>
  }

}

