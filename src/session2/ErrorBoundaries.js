import React from 'react'

class ErrorBoundaries extends React.Component {
  state = {hasError : false}

  static getDerivedStateFromError(error) {
    console.log("My Error", error)
    return {hasError: true}
  }

  // componentDidCatch (error, errorInfo) {
  //   console.log("componentDidCatch", error, errorInfo)
  //   this.setState({hasError: true})
  // }

  render () {
    if (this.state.hasError) {
      return 'Crashed'
    }

    return this.props.children
  }
}

class Child extends React.Component {
  state = {counter: 1}

  render () {
    if (this.state.counter === 4) {
      throw new Error('Crashed app')
    }

    return <span>
      Child
      <button onClick={() => this.setState({counter: this.state.counter + 1})}>{this.state.counter}</button>
    </span>
  }
}

class App extends React.Component {

  render () {
    return <>
      <ErrorBoundaries>
        <Child />
      </ErrorBoundaries>
    </>
  }
}

export default App
