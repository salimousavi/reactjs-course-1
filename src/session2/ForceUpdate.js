import React from 'react'

class ForceUpdate extends React.Component {

  constructor (props) {
    super(props);
    console.log("constructor")
  }

  getRandom() {
    console.log("getRandom")
    this.forceUpdate()
  }

  shouldComponentUpdate (nextProps, nextState, nextContext) {
    return false
  }

  UNSAFE_componentWillMount () {
    console.log("UNSAFE_componentWillMount")
  }

  render () {
    console.log("render")
    return <span>
      {Math.random()}
      <button onClick={() => this.getRandom()}>Get random</button>
    </span>
  }
}

export default ForceUpdate