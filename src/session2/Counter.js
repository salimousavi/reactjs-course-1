import React from 'react';

class Counter extends React.Component {

  constructor (props) {
    super(props);
    console.log(props)
    this.state = {
      counter: props.counter
    }
  }

  render () {
    console.log("render")
    return (
      <div>
        {this.state.counter}
        <button onClick={() => this.setState({counter: this.state.counter + 1})}>Increase</button>
      </div>
    );
  }
}

Counter.defaultProps = {
  counter: 1
}

export default Counter;
