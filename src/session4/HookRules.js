import React, { useState } from 'react'

function App() {
  const [count, setCount] = useState(0)

  // Correct
  const [number, setNumber] = useState(0)

  // Incorrect
  // if (count % 2 === 0) {
  //   const [number, setNumber] = useState(0)
  // }

  // Incorrect
  // for (let i = 0; i < 5; i++) {
  //   const [number, setNumber] = useState(0)
  // }

  // Incorrect
  // function func() {
  //   const [number, setNumber] = useState(0)
  // }

  return (
    <div>
      {count} <button onClick={() => setCount(count => count + 1)}>+</button>
      <hr/>
      {number} <button onClick={() => setNumber(number => number + 1)}>+</button>
    </div>
  )
}

export default App
