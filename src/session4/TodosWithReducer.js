import React, { useReducer, useState } from 'react'

function reducer(state, action) {
  switch (action.type) {
    case 'ADD_TODO':
      return [...state, {id: state.length ? state[state.length - 1].id + 1 : 1, title: action.payload}]

    case 'REMOVE_TODO':
      return state.filter(todo => todo.id !== action.payload)

    default:
      throw new Error('Action type not valid')
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, [])
  const [task, setTask] = useState('')

  function addTodo() {
    if (task === '') return
    dispatch({type: 'ADD_TODO', payload: task})
    setTask('')
  }

  return (
    <div>
      <ol>
        {state.map(todo =>
          <li key={todo.id}>
            {todo.title}
            <span onClick={e => dispatch({type: 'REMOVE_TODO', payload: todo.id})}
                  style={{color: 'red', margin: '10px', cursor: 'pointer'}}>x</span>
          </li>)}
      </ol>
      <input type="text" value={task} onChange={e => setTask(e.target.value)}/>
      <button onClick={addTodo}>Add</button>
    </div>
  )
}

export default App
