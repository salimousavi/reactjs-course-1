import { useEffect, useState } from 'react'

// 6EAB82   =>   0
// 84BA26   =>   1
function App () {

  const [counter, setCounter] = useState(0)

  useEffect(() => {
    console.log('Use Effect')
    const intervalId = setInterval(() => {
      console.log('setInterval', counter)
      setCounter(counter => {
        console.log(counter)
        return counter + 1
      })
    }, 1000)

    return () => {
      clearInterval(intervalId)
    }
  }, [])

  return counter
}

export default App