import React, { useState } from 'react'

// {id: 1, title: 'Go to car wash'}

function App() {
  const [todos, setTodos] = useState([])
  const [task, setTask] = useState('')

  function addTodo() {
    if (task === '') return
    setTodos(todos => [...todos, {id: todos.length ? todos[todos.length - 1].id + 1 : 1,title: task}])
    setTask('')
  }

  function removeTodo(id) {
    setTodos(todos => todos.filter(todo => todo.id !== id))
  }

  return (
    <div>
      <ol>
        {todos.map(todo =>
          <li key={todo.id}>
            {todo.title}
            <span onClick={e => removeTodo(todo.id)}
                  style={{color: 'red', margin: '10px', cursor: 'pointer'}}>x</span>
          </li>)}
      </ol>
      <input type="text" value={task} onChange={e => setTask(e.target.value)}/>
      <button onClick={addTodo}>Add</button>
    </div>
  )
}

export default App
