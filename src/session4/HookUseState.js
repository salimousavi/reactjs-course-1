import React, {useState} from 'react'

function App() {
  const [count, setCount] = useState(0)
  const [name, setName] = useState('Ali')

  return (
    <div>
      {name}
      {count}
      <hr/>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
    </div>
  )
}
//
// function App() {
//
//   const myState = useState(0)
//
//   console.log(myState)
//
//   return (
//     <div>
//       {myState[0]}
//       <hr/>
//       <button onClick={() => myState[1](myState[0] + 1)}>+</button>
//     </div>
//   )
// }

export default App
