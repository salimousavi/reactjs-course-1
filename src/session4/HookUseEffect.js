import React, { useEffect, useState } from 'react'

function App () {
  const [show, setShow] = useState(true)

  return (
    <div>
      <button onClick={() => setShow(!show)}>
        {show ? 'Hide' : 'Show'}
      </button>
      {show && <UseEffect/>}
    </div>
  )
}

function UseEffect () {
  const [counter, setCounter] = useState(0)
  const [count, setCount] = useState(0)
  const [number, setNumber] = useState(0)
  const [users, setUsers] = useState([])

  // cDM, cDU
  // useEffect(() => {
  //   console.log('Call useEffect')
  // })

  // useEffect(() => {
  //   console.log('Component did mount')
  //   const controller = new AbortController()
  //   const signal = controller.signal
  //
  //   fetch('https://jsonplaceholder.typicode.com/users', {signal})
  //     .then(res => res.json())
  //     .then(result => setUsers(result))
  //
  //   return () => {
  //     console.log('Component will unmount')
  //     controller.abort()
  //   }
  // }, [])

  useEffect(() => {
    console.log('setTimeOut', counter)
    const timeoutId = setTimeout(() => setCounter(counter + 1), 3000)

    return () => {
      console.log('Clear previous side effect', counter, timeoutId)
      clearTimeout(timeoutId)
    }
  }, [counter])

  return (
    <div>
      {counter}
      {/*<hr/>*/}
      {/*{count} <button onClick={() => setCount(count + 1)}>+</button>*/}
      {/*<hr/>*/}
      {/*{number} <button onClick={() => setNumber(number + 1)}>+</button>*/}
      {/*<hr/>*/}
      {/*<ul>*/}
      {/*  {users.map(user => <li key={user.id}>{user.name}</li>)}*/}
      {/*</ul>*/}
    </div>

  )
}

export default App