import React, { useEffect, useMemo, useState } from 'react'

function App() {
  const [show, setShow] = useState(true)

  return (
    <div>
      <button onClick={() => setShow(!show)}>
        {show ? 'Hide' : 'Show'}
      </button>
      {show && <UseMemo />}
    </div>
  )
}

function UseMemo() {

  const [counter, setCounter] = useState(0)

  // const [value, setValue] = useState(() => {
  //   return (9999n ** 99999n).toString()
  // })
  //
  // useEffect(() => {
  //   console.log('useEffect')
  //   setValue()
  // }, [counter])

  useEffect(() => {
    setInterval(() => setCounter(counter => counter + 1 ), 1000)
  }, [])

  const myExpensiveValue = useMemo(() => {
    console.log('Use memo')
    return (9999n ** 99999n).toString()
  }, [])


  return (
    <div>
      useMemo
      <hr/>
      {counter}
      <hr/>
      {myExpensiveValue}
    </div>
  )
}

export default App