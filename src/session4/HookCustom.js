import React, { useState } from 'react'

function App() {

  const username = useForm()
  const password = useForm()

  return (
    <div>
      <input type="text" placeholder="Username" {...username}/>
      <input type="text" placeholder="Password" {...password}/>
    </div>
  )
}

function useForm(defaultValue = '') {
  const [value, setValue] = useState(defaultValue)

  return {
    value,
    onChange: e => setValue(e.target.value)
  }
}

// function App() {
//
//   const [username, setUsername] = useState('')
//   const [password, setPassword] = useState('')
//
//   return (
//     <div>
//       <input type="text" placeholder="Username" value={username} onChange={e => setUsername(e.target.value)}/>
//       <input type="text" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
//     </div>
//   )
// }

export default App

