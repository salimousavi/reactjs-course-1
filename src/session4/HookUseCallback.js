import React, { useCallback, useState } from 'react'

function App() {
  const [counter, setCounter] = useState(0)
  const [height, setHeight] = useState(0)

  const focusRef = useCallback(node => {
    if (node) {
      node.focus()
    }
  }, [])

  // const focusRef = (node) => {
  //   if (node) {
  //     node.focus()
  //   }
  //   console.log(node)
  // }

  const heightRef = useCallback((node) => {
    if (node) {
      setHeight(node.getBoundingClientRect().height)
    }
  }, [])

  return (
    <div>
      <h1 ref={heightRef}>My height: {height}</h1>
      <input type="text" ref={focusRef}/>
      <hr/>
      <button onClick={() => setCounter(counter => counter + 1)}>{counter}</button>
    </div>
  )
}

export default App
